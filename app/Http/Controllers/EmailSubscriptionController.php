<?php

namespace App\Http\Controllers;

use App\EmailSubscription;
use App\Http\Requests\EmailSubscriptionRequest;
use Illuminate\Http\Request;

class EmailSubscriptionController extends Controller
{
    public function save(EmailSubscriptionRequest $request){
        EmailSubscription::create($request->all());
        return redirect('/')->with(['success' => true]);

    }
}
