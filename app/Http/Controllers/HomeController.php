<?php

namespace App\Http\Controllers;

use App\VentureCategory;
use App\VentureProject;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function view(){
        return view('index')->with([
            'categories' => VentureCategory::all(),
            'ventures' => VentureProject::all()
        ]);
    }
}
