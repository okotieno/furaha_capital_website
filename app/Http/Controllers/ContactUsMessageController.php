<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactUsMessageRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsMessageController extends Controller
{
    public function sent(ContactUsMessageRequest $request)
    {
        $email_details = $request->all();

        Mail::send('emails.contact-us.confirmation', ['email_details' => $request->all()], function ($m) use ($email_details) {
            $m->from('info@furahacapital.co.ke', 'Furaha Capital Investments Limited');

            $m->to($email_details['email'], $email_details['name'])->subject('Email Receipt Confirmation');
        });

        Mail::send('emails.contact-us.server', ['email_details' => $request->all()], function ($m) use ($email_details) {
            $m->from($email_details['email'], $email_details['name']);

            $m->to('info@furahacapital.co.ke', 'Furaha Capital Investments Limited')->subject('Customer Care Inquiry');
        });
    }
}
