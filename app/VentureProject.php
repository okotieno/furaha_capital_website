<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureProject extends Model
{
    protected  $appends = ['project_date', 'category'];

    protected $hidden = ['created_at', 'updated_at', 'date'];

    protected $dates = ['created_at', 'updated_at', 'date'];

    public function getProjectDateAttribute(){
        return $this->date->format('M Y');
    }

    public function getCategoryAttribute(){
        return $this->ventureCategory->name;
    }

    public function ventureCategory(){
        return $this->belongsTo(VentureCategory::class);
    }

    //
}
