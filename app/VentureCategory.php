<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VentureCategory extends Model
{
    public $timestamps = false;

    public function ventureProject(){
        return $this->hasMany(VentureProject::class);
    }
    //
}
