const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/vitality/assets/js/jquery.js', 'public/js')
    .js('resources/vitality/assets/js/bootstrap/bootstrap.min.js', 'public/js/bootstrap')
    .js('resources/vitality/assets/js/plugins/jquery.easing.min.js', 'public/js/plugins')
    .js('resources/vitality/assets/js/plugins/classie.js', 'public/js/plugins')
    .js('resources/vitality/assets/js/plugins/cbpAnimatedHeader.js', 'public/js/plugins')
    .js('resources/vitality/assets/js/plugins/owl-carousel/owl.carousel.js', 'public/js/plugins/owl-carousel')
    .js('resources/vitality/assets/js/plugins/jquery.magnific-popup/jquery.magnific-popup.min.js', 'public/js/plugins/jquery.magnific-popup/jquery.magnific-popup.min.js')
    .js('resources/vitality/assets/js/plugins/background/core.js', 'public/js/plugins/background')
    .js('resources/vitality/assets/js/plugins/background/transition.js', 'public/js/plugins/background')
    .js('resources/vitality/assets/js/plugins/background/background.js', 'public/js/plugins/background')
    .js('resources/vitality/assets/js/plugins/jquery.mixitup.js', 'public/js/plugins')
    .js('resources/vitality/assets/js/plugins/wow/wow.min.js', 'public/js/plugins/wow/wow.min.js')
    .js('resources/vitality/assets/js/contact_me.js', 'public/js')
    .js('resources/vitality/assets/js/plugins/jqBootstrapValidation.js', 'public/js/plugins')
    .js('resources/vitality/assets/js/vitality.js', 'public/js')
    .js('resources/vitality/assets/js/plugins/retina/retina.min.js', 'public/js/plugins/retina')
    .styles('resources/vitality/assets/css/bootstrap/bootstrap.min.css', 'public/css/bootstrap/bootstrap.min.css')
    .styles('resources/vitality/assets/font-awesome/css/font-awesome.min.css', 'public/font-awesome/css/font-awesome.min.css')
    .styles('resources/vitality/assets/css/plugins/owl-carousel/owl.carousel.css', 'public/css/plugins/owl-carousel/owl.carousel.css')
    .styles('resources/vitality/assets/css/plugins/owl-carousel/owl.theme.css', 'public/css/plugins/owl-carousel/owl.theme.css')
    .styles('resources/vitality/assets/css/plugins/owl-carousel/owl.transitions.css', 'public/css/plugins/owl-carousel/owl.transitions.css')
    .styles('resources/vitality/assets/css/plugins/magnific-popup.css', 'public/css/plugins/magnific-popup.css')
    .styles('resources/vitality/assets/css/plugins/background.css', 'public/css/plugins/background.css')
    .styles('resources/vitality/assets/css/plugins/animate.css', 'public/css/plugins/animate.css')
    .styles('resources/vitality/assets/css/vitality-green.css', 'public/css/vitality-green.css')
;
