@extends('layout.app')
@section('stylesheets')
    <link href="{{ asset('assets/css/bootstrap/bootstrap.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Default Fonts -->
    <link href='https://use.fontawesome.com/releases/v5.3.1/css/all.css' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,600,500,700,800,900' rel='stylesheet' type='text/css'>
    <!-- Modern Style Fonts (Include these is you are using body.modern!) -->
    <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Cardo:400,400italic,700' rel='stylesheet' type='text/css'>
    <!-- Vintage Style Fonts (Include these if you are using body.vintage!) -->
    <link href='http://fonts.googleapis.com/css?family=Sanchez:400italic,400' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Cardo:400,400italic,700' rel='stylesheet' type='text/css'>
    <!-- Plugin CSS -->
    <link href="{{ asset('assets/css/plugins/owl-carousel/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/plugins/owl-carousel/owl.theme.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/plugins/owl-carousel/owl.transitions.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/plugins/magnific-popup.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/plugins/background.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/plugins/animate.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('assets/css/vitality-green.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet" type="text/css">
@endsection
@section('body')
    @include('pages.home')
@endsection
@section('scripts')
    <script src="{{ asset('assets/js/plugins/retina/retina.min.js') }}"></script>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="{{ asset('assets/js/bootstrap/bootstrap.min.js') }}"></script>
    <!-- Plugin Scripts -->
    <script src="{{ asset('assets/js/plugins/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/classie.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/cbpAnimatedHeader.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/owl-carousel/owl.carousel.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/background/core.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/background/transition.j') }}s"></script>
    <script src="{{ asset('assets/js/plugins/background/background.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jquery.mixitup.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/wow/wow.min.js') }}"></script>
    <script src="{{ asset('assets/js/contact_me.js') }}"></script>
    <script src="{{ asset('assets/js/plugins/jqBootstrapValidation.js') }}"></script>
    <!-- Vitality Theme Scripts -->
    <script src="{{ asset('assets/js/vitality.js') }}"></script>
@endsection
