<section id="pricing" class="pricing" style="background-image: url('assets/img/bg-pricing.jpg')">
    <div class="container wow fadeIn">
        <div class="row text-center">
            <div class="col-lg-12">
                <h2>Invest</h2>
                <hr class="colored">
                <p>Join us in building a future of solutions in a world of opportunities. You can invest in us through Grants, Strategic Partnerships or Equity to scale and expand our social enterprises portfolio</p>
            </div>
        </div>
        <div class="row content-row">
            <div class="col-md-4">
                <div class="pricing-item featured-first">
                    <h3>GRANTS</h3>
                    <hr class="colored">
                    <ul class="list-group">
                        <li class="list-group-item">Grant utilisation reports</li>
                        <li class="list-group-item">Progress reports</li>
                        <li class="list-group-item">Impact evaluation</li>
                        <li class="list-group-item">Participation in grants’ forums</li>
                        <li class="list-group-item">Sharing lessons with sector ecosystems</li>
                    </ul>
                    <a href="#contact" class="page-scroll btn btn-outline-dark">Engage</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="pricing-item featured">
                    <h3>STRATEGIC PARTNERSHIPS</h3>
                    <hr class="colored">
                    <ul class="list-group">

                        <li class="list-group-item">Business synergies</li>
                        <li class="list-group-item">Scaling to public sector</li>
                        <li class="list-group-item">Innovation at scale</li>
                        <li class="list-group-item">Solution implementation catalyst</li>

                    </ul>
                    <a href="#contact" class="page-scroll btn btn-outline-dark">Engage</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="pricing-item featured-last">
                    <h3>EQUITY</h3>
                    <hr class="colored">
                    <ul class="list-group">
                        <li class="list-group-item">Shareholder value maximization</li>
                        <li class="list-group-item">Progress reports</li>
                        <li class="list-group-item">Revenue growth</li>
                    </ul>
                    <a href="#contact" class="page-scroll btn btn-outline-dark">Engage</a>
                </div>
            </div>
        </div>
    </div>
</section>
