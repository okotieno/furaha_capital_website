<section id="process" class="services" style="background: #BBFFFF">
    <div class="container">
        <div class="row text-center">
            <div class="col-lg-12 wow fadeIn">
                <h2>Our Process</h2>
                <hr class="colored">
                <p>Here is an overview of how we approach each new venture.</p>
            </div>
        </div>
        <div class="row content-row">
            <div class="col-md-4 wow fadeIn" data-wow-delay=".2s">
                <div class="media">
                    <div class="pull-left">
                        <i class="fa fa-clipboard" style="color: white; background: #AE7200; border: 2px solid white"></i>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading" style="color: darkblue">Ideate</h3>
                        <ul>
                            <li>Problem scoping research</li>
                            <li>Initial strategy development</li>
                            <li>Product development initiation</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 wow fadeIn" data-wow-delay=".4s">
                <div class="media">
                    <div class="pull-left">
                        <i class="fa fas fa-pencil-alt" style="color: white; background: #AE7200; border: 2px solid white" ></i>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading" style="color: darkblue">Prototype</h3>
                        <ul>
                            <li>Build solution framework</li>
                            <li>Gather client feedback</li>
                            <li>Incorporate feedback</li>
                            <li>Pilot</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-4 wow fadeIn" data-wow-delay=".6s">
                <div class="media">
                    <div class="pull-left">
                        <i class="fa fa-rocket" style="color: white; background: #AE7200; border: 2px solid white"></i>
                    </div>
                    <div class="media-body">
                        <h3 class="media-heading" style="color: darkblue">Launch</h3>
                        <ul>
                            <li>Target market introduction</li>
                            <li>Collect UX data</li>
                            <li>Optimise usage</li>
                            <li>Iterate as we scale</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
