<section id="contact" style="background: lightblue">
    <div class="container wow fadeIn">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>Contact Us</h2>
                <hr class="colored">
                <p>Please tell us about your enterprise venture plans or investment preferences. We are happy to hop on
                    a call to learn how we can collaborate.</p>
            </div>
        </div>
        <div class="row content-row">
            <div class="col-lg-8 col-lg-offset-2">
                <form name="sentMessage" id="contactForm" method="post" action="/message/sent">
                    {{ csrf_field() }}
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Name</label>
                            <input name="name" type="text" class="form-control" placeholder="Name" id="name" required
                                   data-validation-required-message="Please enter your name.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Email Address</label>
                            <input name="email" type="email" class="form-control" placeholder="Email Address" id="email"
                                   required
                                   data-validation-required-message="Please enter your email address.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Phone Number</label>
                            <input name="phone" type="tel" class="form-control" placeholder="Phone Number" id="phone"
                                   required
                                   data-validation-required-message="Please enter your phone number.">
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <div class="row control-group">
                        <div class="form-group col-xs-12 floating-label-form-group controls">
                            <label>Message</label>
                            <textarea name="message" rows="5" class="form-control" placeholder="Message" id="message"
                                      required
                                      data-validation-required-message="Please enter a message."></textarea>
                            <p class="help-block text-danger"></p>
                        </div>
                    </div>
                    <br>
                    <div id="contact-us-progress" class="progress active" hidden="hidden">
                        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                             aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%"></div>
                    </div>
                    <div id="success"></div>
                    <div class="row">
                        <div class="form-group col-xs-12">
                            <button id="submit-contact-us" type="submit" class="btn btn-outline-dark">Send</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<style>
    .controls {
        margin-bottom: 4px;
        border-left: #0e3950 5px solid
    }
</style>
