<section class="bg-gray" style="background: #BBFFFF">
    <div class="container text-center wow fadeIn">
        <div class="row">
            <div class="col-lg-12">
                <h2>Our Team</h2>
                <p>
                    Investment in business creation is a highly interdisciplinary field that draws on approaches and insights from economics, finance, technology, psychology, data science and more. Our team combines research with strong private sector experience to build evidence-driven yet pragmatic enterprises. Our 30+ ventures teams are professionally diverse to ensure our solutions are leveraging cutting edge business science.
                </p>
                <hr class="colored">
            </div>
        </div>
        <div class="row content-row">
            <div class="col-lg-12">
                <div class="about-carousel">
                    <div class="item">
                        <img src="assets/img/people/1.jpg" class="img-responsive" alt="">
                        <div class="caption">
                            <h3 style="">Kennedy Kahura</h3>
                            <hr class="colored">
                            <p>Chief Executive Officer</p>
                            <ul class="list-inline social">
                                <li>
                                    <a style="color: #0A8FD5" href="https://www.facebook.com/kkahura"><i
                                            class="fab fa-facebook-f fa-fw"></i></a>
                                </li>
                                <li>
                                    <a style="color: #0A8FD5" href="https://twitter.com/ken_kahura"><i
                                            class="fab fa-twitter fa-fw"></i></a>
                                </li>
                                <li>
                                    <a style="color: #0A8FD5" href="https://www.linkedin.com/in/ken-kahura"><i
                                            class="fab fa-linkedin fa-fw"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <img src="assets/img/people/2.jpg" class="img-responsive" alt="">
                        <div class="caption">
                            <h3>Owen Kelvin</h3>
                            <hr class="colored">
                            <p>Managing Director</p>

                            <ul class="list-inline social">
                                <li>
                                    <a href="https://www.facebook.com/okonyots3"><i class="fab fa-facebook-f fa-fw"></i></a>
                                </li>
                                <li>
                                    <a href="https://bitbucket.org/okotieno/"><i class="fab fa-bitbucket fa-fw"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/in/otieno-owen-62ba6641/"><i
                                            class="fab fa-linkedin fa-fw"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="item">
                        <img src="assets/img/people/2.jpg" class="img-responsive" alt="">
                        <div class="caption">
                            <h3>Owen Kelvin</h3>
                            <hr class="colored">
                            <p>Business Development Manager</p>

                            <ul class="list-inline social">
                                <li>
                                    <a href="https://www.facebook.com/okonyots3"><i class="fab fa-facebook-f fa-fw"></i></a>
                                </li>
                                <li>
                                    <a href="https://bitbucket.org/okotieno/"><i class="fab fa-bitbucket fa-fw"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/in/otieno-owen-62ba6641/"><i
                                            class="fab fa-linkedin fa-fw"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
