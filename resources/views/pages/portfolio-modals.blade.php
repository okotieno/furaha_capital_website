@foreach($ventures as $venture)
    <div class="portfolio-modal modal fade" id="portfolioModal{{ $venture['id'] }}" tabindex="-1" role="dialog" aria-hidden="true"
         style="background-image: url('assets/img/portfolio/{{ $venture['background'] }}')">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <img src="assets/img/client-logos/{{ $venture['logo'] }}" class="img-responsive img-centered" alt="">
                            <h2>{{ $venture['name'] }}</h2>
                            <hr class="colored">
                            <p>
                                {{ $venture['long_description'] }}
                            </p>
                        </div>
                        <div class="col-lg-12">
                            <p>
                                {!! $venture['youtube'] !!}
                            </p>
                            <p>
                                <img src="assets/img/portfolio/{{ $venture['image'] }}"
                                     class="img-responsive img-centered"
                                     alt="">
                            </p>

                        </div>
                        <div class="col-lg-8 col-lg-offset-2">
                            <ul class="list-inline item-details">
                                <li>
                                    Venture: <strong><a href="{{ $venture['link'] }}">
                                            {{ $venture['client'] }}
                                        </a></strong>
                                </li>
                                <li>Date: <strong><a href="{{ $venture['link'] }}">{{ $venture['project_date'] }}</a></strong>
                                </li>
                                <li>Mission: <strong><a href="{{ $venture['link'] }}">{{ $venture['service'] }}</a></strong>
                                </li>
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i
                                    class="fa fa-times"></i>
                                Close
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endforeach
