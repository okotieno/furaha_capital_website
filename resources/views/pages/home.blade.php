@include('layout.navigation-bar')
<header style="background-image: url('assets/img/bg-header.jpg');">
    <div class="intro-content">
        <img src="assets/img/profile.png" class="img-responsive img-centered" alt="">
        <div class="brand-name">Furaha Capital</div>
        <hr class="colored">
        <div class="brand-name-subtext">You are the best, You deserve the best</div>
    </div>
    <div class="scroll-down">
        <a class="btn page-scroll" href="#about"><i class="fa fa-angle-down fa-fw"></i></a>
    </div>
</header>
<section id="about" style="background: #BBFFFF">
    <div class="container-fluid">
        <div class="row text-center">
            <div class="col-lg-12 wow fadeIn">
                <h1>It's all investments & the best products</h1>
                <p>
                    We create value in sectors with the greatest impact to economies by leveraging technological
                    innovation. Our focus is in building startups from the ground up. We build capacity, systems and
                    execute on sound strategies to ensure sustainable value creation. Resources are mobilised internally
                    to iterate through the high risk stages of product development, prototyping and launch. Once we have
                    a viable business, we seek out external partners who are aligned with the vision of the company to
                    accelerate growth.
                </p>
                <hr class="colored">
            </div>
        </div>
        <div class="row text-center content-row">
            <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay=".2s">
                <div class="about-content">
                    <i class="fa fa-space-shuttle fa-4x" style="color: #AE7200"></i>
                    <h3 style="color: darkblue">Innovation</h3>
                    <p>
                    </p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay=".4s">
                <div class="about-content">
                    <i class="fas fa-city fa-4x" style="color: #AE7200"></i>
                    <h3 style="color: darkblue">Creativity</h3>
                    <p></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay=".6s">
                <div class="about-content">
                    <i class="fas fa-users fa-4x" style="color: #AE7200"></i>
                    <h3 style="color: darkblue">Relations</h3>
                    <p></p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 wow fadeIn" data-wow-delay=".8s">
                <div class="about-content">
                    <i class="far fa-chart-bar fa-4x" style="color: #AE7200"></i>
                    <h3 style="color: darkblue">Growth</h3>
                    <p></p>
                </div>
            </div>
        </div>
    </div>
</section>

@include('pages.our-team')

<aside class="cta-quote" style="background-image: url('assets/img/bg-aside.jpg');">
    <div class="container wow fadeIn">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <span class="quote">Good <span class="text-primary">Investment</span> is putting your money where <span
                        class="text-primary">returns</span> are maximum and the <span class="text-primary">risk</span> is minimal.</span>
                <hr class=" colored">
                <a class="btn btn-outline-light page-scroll" href="#contact">Let's Invest</a>
            </div>
        </div>
    </div>
</aside>
@include('pages.process')

<section id="work" class="bg-gray">
    <div class="container text-center wow fadeIn">
        <h2>Our Ventures</h2>
        <hr class="colored">
        <p>Examples of our work which represents our business creation capabilities.</p>
    </div>
</section>
<section class="portfolio-carousel wow fadeIn">

    @foreach($ventures->where('main_page', true) as $venture)
        <div class="item" style="background-image: url('assets/img/portfolio/{{ $venture['background'] }}')">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4 col-md-push-8">
                        <div class="project-details">
                            <img src="assets/img/client-logos/{{ $venture['logo'] }}" class="img-responsive client-logo"
                                 alt="">
                            <span class="project-name">{{ $venture['name'] }}</span>
                            <span class="project-description">{{ $venture['short_description'] }}</span>
                            <hr class="colored">
                            <a href="#portfolioModal{{ $venture['id'] }}" data-toggle="modal"
                               class="btn btn-outline-light">View Details <i
                                    class="fa fa-long-arrow-right fa-fw"></i></a>
                        </div>
                    </div>
                    <div class="col-md-8 col-md-pull-4 hidden-xs">
                        <img src="assets/img/portfolio/{{ $venture['image'] }}" class="img-responsive portfolio-image"
                             alt="">
                    </div>
                </div>
            </div>
        </div>
    @endforeach

</section>

<!-- Portfolios  -->

@include('pages.portfolio')

<!--endPortfolios-->

<!-- Testimonials will be included here -->

<!--endTestimonial-->

@include('pages.invest')

<section class="cta-form bg-dark">
    <div class="container text-center">
        <h3>Subscribe to our newsletter!</h3>
        <hr class="colored">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <!-- MailChimp Signup Form -->
                <div id="mc_embed_signup">
                    <!-- Replace the form action in the line below with your MailChimp embed action! For more informatin on how to do this please visit the Docs! -->
                    <form role="form" action="{{ asset('/subscribe/newsletter') }}" method="post"
                          id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form">
                        <div class="input-group input-group-lg">
                            {{ csrf_field() }}
                            <input type="email" name="email" class="form-control" id="mce-EMAIL"
                                   placeholder="Email address...">
                            <span class="input-group-btn">
                                    <button type="submit" name="subscribe" id="mc-embedded-subscribe"
                                            class="btn btn-primary">Subscribe!</button>
                                </span>
                        </div>
                        <div id="mce-responses">
                            <div class="response" id="mce-error-response" style="display:none"></div>
                            <div class="response" id="mce-success-response" style="display:none"></div>
                        </div>
                    </form>
                </div>
                <!-- End MailChimp Signup Form -->
            </div>
        </div>
    </div>
</section>
@include('pages.contact-us')

@include('layout.footer')
<!-- Portfolio Modals -->
@include('pages.portfolio-modals')
