<section>
    <div class="container text-center wow fadeIn">
        <h2>Portfolio</h2>
        <hr class="colored">
        <p>Here are some other projects that we've worked on.</p>
        <div class="row content-row">
            <div class="col-lg-12">
                <div class="portfolio-filter">

                    <ul id="filters" class="clearfix">
                        <li>
                            <span class="filter active"
                                  data-filter="{{ implode(" ", $categories->pluck('name')->toArray()) }}">
                                All
                            </span>
                        </li>
                        @foreach($categories as $category)
                            <li>
                                <span class="filter" data-filter="{{ $category->name }}">{{ $category->name }}</span>
                            </li>
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="portfoliolist">
                    @foreach($ventures->where('main_page', true) as $venture)
                        <div class="portfolio {{ $venture->category }}" data-cat="{{ $venture->category }}"
                             href="#portfolioModal{{ $venture->id }}" data-toggle="modal">
                            <div class="portfolio-wrapper">
                                <img width="500" height="200" src="assets/img/portfolio/{{ $venture->image }}" alt="">
                                <div class="caption">
                                    <div class="caption-text">
                                        <a class="text-title">{{ $venture->name }}</a>
                                        <span class="text-category">{{ $venture->short_description }}</span>
                                    </div>
                                    <div class="caption-bg"></div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
