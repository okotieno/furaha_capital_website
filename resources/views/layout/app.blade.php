<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <title>Furaha Capital</title>
    <link rel="icon" type="image/png" href="/assets/img/profile.png">

    @yield('stylesheets')

</head>
<body id="page-top">
@yield('body')
@yield('scripts')
</body>
</html>
