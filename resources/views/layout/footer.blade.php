<a href="#contact" class="btn btn-block btn-full-width page-scroll">Invest in Furaha Capital Now!</a>
<footer class="footer" style="background-image: url('assets/img/bg-footer.jpg')">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-4 contact-details">
                <h4><i class="fa fa-phone"></i> Call</h4>
                <p>+254 (0) 734 045 321</p>
                <p>+254 (0) 738 839 778</p>
            </div>
            <div class="col-md-4 contact-details">
                {{--<h4><i class="fa fa-map-marker"></i> Visit</h4>--}}
                {{--<p>3481 Melrose Place--}}
                    {{--<br>Beverly Hills, CA 90210</p>--}}
            </div>
            <div class="col-md-4 contact-details">
                <h4><i class="fa fa-envelope"></i> Email</h4>
                <p><a href="mailto:mail@example.com">info@furahacapital.co.ke</a>
                </p>
            </div>
        </div>
        <div class="row social">
            <div class="col-lg-12">
                <ul class="list-inline">
                    <li><a href="https://www.facebook.com/FurahaCapital-515580285574109/"><i class="fab fa-facebook-f fa-fw fa-2x"></i></a>
                    </li>
                    {{--<li><a href="#"><i class="fa fa-twitter fa-fw fa-2x"></i></a>--}}
                    {{--</li>--}}
                    <li><a href="https://www.linkedin.com/company/furaha-capital/"><i class="fab fa-linkedin fa-fw fa-2x"></i></a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row copyright">
            <div class="col-lg-12">
                <p class="small">&copy; 2015 Furaha Capital Investment Limited</p>
            </div>
        </div>
    </div>
</footer>
